from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=200)
    car_make = models.CharField(max_length=200)

    def __str__(self):
        return ','.join([str(self.name), str(self.car_make)])
