from people.models import Person
from tastypie.resources import ModelResource


class PersonResource(ModelResource):

    class Meta:
        queryset = Person.objects.all()
        resource_name = 'person'
        limit = 0
        max_limit = 0
