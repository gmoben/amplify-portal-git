from django.conf.urls import patterns, include, url
from django.contrib import admin

from tastypie.api import Api
from apps.people.api import PersonResource

admin.autodiscover()

# Tastypie API
api = Api(api_name='api')
api.register(PersonResource())

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'backend.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(api.urls))
)
