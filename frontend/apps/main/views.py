from django.shortcuts import render
import requests


def show_homepage(request):
    ''' Fetch data, build context and render `index.html`'''

    people = requests.get(
        'http://localhost:8500/api/person/').json()['objects']
    data = [x['car_make'] for x in people]
    counts = {x: data.count(x) for x in set(data)}

    return render(request, 'index.html',
                  {'people': people, 'data': data, 'counts': counts})
